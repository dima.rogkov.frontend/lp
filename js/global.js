// JS MENU
// ==============
// - HEADER
// - MUSIC WAVE
// - STORE SLIDE
// ==============

$(document).ready(function() {

	"use strict";

	// INIT SWIPER ON MOBILE
	const initSwiperOnMobile = (swiperBlock, swiperOptions, swipeBreakpoint) => {
		let breakpoint = window.matchMedia( `(max-width: ${swipeBreakpoint})` ),
			swiper;

		const breakpointChecker = () => {
			if (breakpoint.matches) {
				return swiper = new Swiper(swiperBlock, swiperOptions);
			} else {
				if (swiper) {
					return swiper.destroy(true, true);
				}
			}
		};

		breakpoint.addListener(breakpointChecker);
		breakpointChecker();
	}

	// INIT MARQUEE
	const initMarquee = (item, options) => {
		item.marquee(options);
	}

	// HEADER -> open menu
	$(document).on('click', '.header-burger', function(e) {
		e.preventDefault();
		const th = $(this);

		$('body').toggleClass('lock');
		th.closest('.header').toggleClass('open-menu');
	});

	// HEADER -> close menu
	$(document).on('click', function(e) {
		const header = $('.header');
		const burgerBtn = $('.header-burger');

 		if (header.hasClass('open-menu')) {
	 		if (!burgerBtn.is(e.target) && burgerBtn.has(e.target).length === 0) {
	 			$('body').removeClass('lock');
	 			header.removeClass('open-menu');
	 		};
	 	};
 	});

	// MUSIC WAVE
	$('.music-item').each(function () {
		const item = $(this);
		const itemId = item.attr('id');
		const itemSong = item.attr('data-song');
		const itemBtn = item.find('.wavesurfer-btn');
		const wave = WaveSurfer.create({
			container: `#${itemId} .wavesurfer`,
			height: $(window).width() > 575 ? 60 : 50,
			waveColor: '#f5f5f5',
			cursorColor: '#b02929',
			progressColor: '#b02929',
			cursorWidth: $(window).width() > 575 ? 3 : 2,
			barWidth: $(window).width() > 575 ? 3 : 2,
			barRadius: $(window).width() > 575 ? 3 : 2,
			barGap: $(window).width() > 575 ? 8 : 6,
			partialRender: true,
			responsive: true
		});

		wave.load(`../../music/${itemSong}`);

		// play & pause
		itemBtn.on('click', function (e) {
			e.preventDefault();
			const th = $(this);

			if (th.hasClass('play')){
				th.removeClass('play').addClass('pause');
				wave.play();
			} else  {
				th.removeClass('pause').addClass('play');
				wave.pause();
			}
		});

		// when music is finish
		wave.on('finish', function () {
			itemBtn.removeClass('pause').addClass('play');
			wave.stop();
		});

	});

	// STORE SLIDE
	const storeSection = $('.store-sec');
	if (storeSection) {
		const initSwiperBreakpoint = storeSection.attr('data-init-swiper-on');
		const swiperOptions = {
			speed: 700,
			observer: true,
			loop: true,
			centeredSlides: true,
			autoplay: {
				delay: 4000,
				disableOnInteraction: false,
				pauseOnMouseEnter: true,
			},
			pagination: {
				el: '.swiper-pagination',
				type: 'bullets',
				clickable: true
			},
			breakpoints: {
				320: {
					slidesPerView: 1
				},
				575: {
					slidesPerView: 2
				},
				767: {
					slidesPerView: 3
				}
			}
		};

		initSwiperOnMobile(".store-block", swiperOptions, initSwiperBreakpoint);
	}

	// ALBUM SECTION
	const albumSection =  $('.album-sec');
	if (albumSection) {
		const marqueeItem = $('.album-items');
		const marqueeOptions = {
			direction: 'up',
			duration: 5000,
			duplicated: true,
			gap: 0
		}

		initMarquee(marqueeItem, marqueeOptions);
	}

});
